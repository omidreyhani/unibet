import { FETCH_MATCHES, FETCH_MATCHES_FAILURE } from '../constants/action-types';
import {get} from '../api';

const REACT_APP_APP_ID = process.env.REACT_APP_APP_ID;
const REACT_APP_APP_KEY = process.env.REACT_APP_APP_KEY;

export const url = `http://api.unicdn.net/v1/feeds/sportsbook/event/live.jsonp?app_id=${REACT_APP_APP_ID}&app_key=${REACT_APP_APP_KEY}`;

export const getAllMatches = () => dispatch => {
    get(url).then(response => {
        return response;
    }, error => {
        return dispatch({ type: FETCH_MATCHES_FAILURE, payload: error.message });
    }).then(json => {
        return dispatch({
            type: FETCH_MATCHES,
            payload: json
        });
    }).catch(error => {
        return dispatch({ type: FETCH_MATCHES_FAILURE, payload: error.message });
    });
}