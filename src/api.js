import fetchJsonp from 'fetch-jsonp';
import moment from 'moment';

const timeSeconds = 2 * 60;

const setItem = (key, data) => {
    localStorage.setItem(key + '_api_timeout', new Date().valueOf());
    localStorage.setItem(key, JSON.stringify(data));
}

const getItem = (key) => {
    const data = localStorage.getItem(key);
    if (data) return JSON.parse(data);
}

const isOld = (key) => {
    const timeout = parseInt(localStorage.getItem(key + '_api_timeout'));
    const old = moment(timeout).add(timeSeconds, 'seconds').isSameOrAfter(new Date);
    return !old;
}

export const get = (url) => {
    if (isOld(url)) {
        return fetchJsonp(url).then(result => {
            result.json().then(value => {
                setItem(url, value);
            });
            return result.json();
        });
    } else {
        return Promise.resolve(getItem(url));
    }
}

