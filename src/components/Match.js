import React  from 'react';
import './Match.css';

const url='https://www.unibet.com/betting#/event/live/';

const Match = (props) => {

    const { home, away, time, name, sport,id } = props.event;
    const className = 'sport ' + sport;
    const href=`${url}${id}`;

    return <div className="match">
        <div className="score">{home} - {away}</div>
        <div className="name"><i className={className}>&nbsp;</i>{name}</div>
        <div className="time">{time}</div>
        <div className="bet">
            <a href={href}>Place a bet</a>
        </div>
    </div>;
}

export default Match 