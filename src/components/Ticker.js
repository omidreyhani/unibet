import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Match from './Match';
import "./Ticker.css";

const everyMatch = 3000;
const animationMatch = 500;

export default class Ticker extends Component {
    constructor(props) {
        super(props);
        this.move = this.move.bind(this);
        this.state = { active: 0 };
    }
    
    componentDidMount() {
        this.timer = setInterval(this.move, everyMatch+animationMatch);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    move() {
        const { events } = this.props;
        let active = this.state.active;
        active++;
        active = active >= events.length ? 0 : active;
        this.setState({ active: active });
    }
    render() {
        const { events } = this.props;
        const { active } = this.state;

        let results = [];
        if (events && events.length > 0) {
            for (let i = active; i < active + 1; i++) {
                let index = i
                if (i < 0) {
                    index = events.length + i
                } else if (i >= events.length) {
                    index = i % events.length
                }
                results.push(<Match key={index} event={events[i]} />)
            }

            return <div className="ticker">
                <ReactCSSTransitionGroup transitionName="left" transitionEnterTimeout={0} transitionLeaveTimeout={0}>
                    {results}
                </ReactCSSTransitionGroup>
            </div>
        }
        return null;
    }
}

