describe('Ticker that shows live matches based on API data.', () => {
    it('"ticker" element that shows matches that are currently being played.', () => {

    })

    it('It should show one match at a time and use a carousel type sliding animation to switch between them.', () => {

    })


    it('Only one match should be shown at a time', () => { });
    it('Every match should be shown for 3 seconds', () => { });
    it('After this it should switch to a new match using a "carousel" style animation where the current match slides out left and the new match slides in', () => { });
    it('The duration of the slide animation should be 500 ms', () => { });
    describe('Each match should show', () => {
        it('The current score (home team - away team)', () => { });
        it('The teams playing (home team - away team)', () => { });
        it('An icon representing the sport, if the sport is <strong>Football, Tennis or Basketball</strong>. For any other sport a default icon should be shown.', () => { });
        it('The date and time when match starts/started. If the match is playing today it should say <strong>"Today"</strong>, otherwise it should show the date in <code>YYYY-MM-DD</code> format.', () => { });
        it('A button style link that opens the match in the Unibet Betting Client. The format of the link should be <code>https://www.unibet.com/betting#/event/live/1234</code>, where 1234 is replaced with the correct Event ID for the match.', () => { });
    });

    it('The data fetched from the API should be <strong>cached for 2 minutes</strong>. Meaning that no additional API calls should be made during this time, even when the page is reloaded', () => {

    });


})