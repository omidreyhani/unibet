import React, { Component } from 'react';
import Ticker from '../components/Ticker';
import {connect} from 'react-redux';
import './main.css';


class App extends Component {
  render() {
    return (
      <div>

        <header>
          <div id="logo">Unibet</div>
        </header>
        <nav>
          <ul>
            <li><a href="instructions/index.html">Test instructions</a></li>
          </ul>
        </nav>
        <div id="content">
          <article>
            <h1>Live matches</h1>
            <p className="preamble">
              Here is a list of matches that are live right now.
                    </p>
                    <p className="error">
                    {this.props.error && 'Something went wrong. Please try again letter.'}
                    </p>
                    <Ticker events={this.props.events}></Ticker>
            <aside>
              <h2>Live betting</h2>
              <p>Place your bets as the action unfolds. We offer a wide selection of live betting events and you can place both single and combination bets.</p>
              <p>You will be able to see an in-play scoreboard with the current result and match stats, while on selected events you will also be able to watch the action live with Unibet TV on the desktop site.</p>
            </aside>
            <div id="live-matches"></div>
          </article>
        </div>
        <footer>
          <div className="inner">
            <p>&copy; 1997-2015, Unibet. All rights reserved.</p>
          </div>
        </footer>

      </div>
    );
  }
}

function mapStateToProps(state) {
    return { events:state.matchReducer.events,
            error:state.matchReducer.error
     }
  }
  
  function mapDispatchToProps(dispatch) {
    return {  }
  }

export default connect(mapStateToProps,mapDispatchToProps)(App);
