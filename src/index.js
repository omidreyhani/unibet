import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/Root';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import {getAllMatches} from './actions/getAllMatches'
import store from './store'

store.dispatch(getAllMatches());

ReactDOM.render(<Provider store={store}>
                    <App/>
                </Provider>, document.getElementById('container'));
                
registerServiceWorker();


