import {combineReducers} from 'redux';
import matchReducer from './match';

export default combineReducers({matchReducer});
