import { FETCH_MATCHES, FETCH_MATCHES_FAILURE } from '../constants/action-types';
import moment from 'moment';

const initialState = {
    events: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MATCHES:
            return {
                ...state,
                events: action.payload.liveEvents.map(live => ({
                    home: live.liveData.score && live.liveData.score.home,
                    away: live.liveData.score && live.liveData.score.away,
                    name: live.event.name,
                    sport: live.event.sport.toLowerCase(),
                    id: live.event.id,
                    time: moment(live.event.start).isSame(moment(), 'day') ? moment(live.event.start).format('[Today] HH:mm') :
                        moment(live.event.start).format('YYYY-MM-DD')
                }))
            };
        case FETCH_MATCHES_FAILURE:
            return {
                ...state,
                events: [],
                error: action.payload
            }

        default:
            return state;
    }
}